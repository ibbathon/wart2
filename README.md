# Warframe Relic Tracker 2 (WaRT 2)
An inventory tracker and relic runner for Warframe, allowing you to easily track which prime part you should be choosing in each run.

This is a ground-up remake of [WaRT](https://github.com/ibbathon/wart) with 3 main goals:
1. Learn new libraries. My current employer is transitioning to FastAPI, so I want to explore the features of that library. I also want to learn a job-scheduler library and a new frontend library (probably Mithril).
2. Add a bunch of the features that I wanted in the original WaRT but never got around to implementing. For example: cookie-less logins, Warframe Market integration, possibly a way of automatically detecting relic rewards screen, etc.
3. Implement unit, integration, and system testing, with the intent of eventually having CI/CD. I've never actually managed that on a personal project and none of the companies I've worked at have had CD or even good CI.

Each part of this app will be deployable as a docker container and I will be posting the docker images to a public Docker Hub location, so anyone can deploy their own version. If a desktop app is required (e.g. for the relic rewards screen reader), then that will be deployed as a packaged Python app.
